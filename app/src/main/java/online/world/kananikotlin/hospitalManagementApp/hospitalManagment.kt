package online.world.kananikotlin.hospitalManagementApp

import online.world.kananikotlin.previousSessionPractices.a
import java.lang.NumberFormatException
import kotlin.system.exitProcess

var patientsArray = arrayOfNulls<String>(100)
var doctorsArray = arrayOfNulls<String>(100)
var i = 0
var c = 0

fun main() {
    println("Welcome")
    showMainMenu()
}

fun showMainMenu() {

    println("\n**************Main Tab**************\n")
    println(
        """1) Patients Management
        |2) Doctors Management
        |3) Exit
    """.trimMargin()
    )
    print("Enter a Number: ")
    var num: Int = 0
    try {
        num = readLine()?.toInt() ?: 0
    } catch (e: NumberFormatException) {
        println("Enter Correct Number")
        showMainMenu()
    }

    when (num) {
        0 -> showMainMenu()
        1 -> showPatientsMenu()
        2 -> showDoctorsMenu()
        3 -> exit()
        else -> showMainMenu()
    }

}

fun showDoctorsMenu() {

    println("\n\n**************Doctors Tab**************\n")
    println(
        """1) Add a Doctor
        |2) Edit a Doctor Info
        |3) Delete a Doctor
        |4) Show All Doctors
        |5) Go To Main Menu
        |6) Exit
    """.trimMargin()
    )

    print("Enter a Number: ")

    var pNum: Int = 0

    try {
        pNum = readLine()?.toInt() ?: 0
    } catch (e: NumberFormatException) {
        println("Enter Correct Number")
        showPatientsMenu()
    }

    when (pNum) {
        0 -> showPatientsMenu()
        1 -> addDoctor()
        2 -> editDoctor()
        3 -> deleteDoctor()
        4 -> showDoctors()
        5 -> showMainMenu()
        6 -> exit()
        else -> showMainMenu()
    }


}

fun showDoctors() {

    var isAllNull = true

    for (b in doctorsArray.indices) {
        if (doctorsArray[b] != null) {
            println(doctorsArray[b])
            isAllNull = false
        }

    }

    if (isAllNull) {
        println("Nothing To Show\n\n")
    }

    showDoctorsMenu()

}

fun deleteDoctor() {

    print("Enter Doctor Name: ")
    val eName = readLine()
    if (eName == "") {

        println("Enter Correct Name")
        editPatient()

    } else {

        var isFound = false

        for ((i, namee) in doctorsArray.withIndex()) {

            if (namee != null) {
                if (namee.contains(eName.toString())) {

                    doctorsArray[i] = null
                    println("Doctor Delete")
                    isFound = true
                    break

                }
            }

        }
        if (!isFound)
            println("Doctor Not Found")

    }
    showDoctorsMenu()

}

fun editDoctor() {

    print("Enter Doctor Name: ")
    val eName = readLine()
    if (eName == "") {

        println("Enter Correct Name")
        editDoctor()

    } else {

        var isFound = false

        for ((i, name) in doctorsArray.withIndex()) {

            if (name != null) {
                if (name.contains(eName.toString())) {

                    print("Doctor Found, Enter New Name: ")
                    val newName: String? = readLine() ?: "Unknown Name"

                    print("Enter New Doctor Number: ")
                    var newNum: Int? = 0
                    try {

                        newNum = readLine()?.toInt() ?: 0

                    } catch (e: NumberFormatException) {

                    }

                    doctorsArray[i] = "$newName  doctor id = $newNum"
                    println("Doctor Edit")
                    isFound = true
                    break

                }
            }

        }
        if (!isFound)
            println("Nothing Found")

    }
    showDoctorsMenu()

}

fun addDoctor() {

    print("Enter Doctor Name: ")
    val name: String = readLine()?.toString() ?: "Unknown Name"
    if (name != "") {

        var patientNum: Int? = 0

        print("Enter Doctor Number: ")
        try {

            patientNum = readLine()?.toInt() ?: 0

        } catch (e: NumberFormatException) {

            println("Enter Correct Number")
            addDoctor()

        }

        doctorsArray[i] = "$name  doctor id = $patientNum"
        ++i
        println("doctor Add")
        showPatientsMenu()
    } else {
        println("Enter Correct Name")
        addDoctor()
    }

}

fun deletePatient() {

    print("Enter Patient Name: ")
    val eName = readLine()
    if (eName == "") {

        println("Enter Correct Name")
        editPatient()

    } else {

        var isFound = false

        for ((i, name) in patientsArray.withIndex()) {

            if (name != null) {
                if (name.contains(eName.toString())) {

                    patientsArray[i] = null
                    println("Patient Delete")
                    isFound = true
                    break

                }
            }

        }
        if (!isFound)
            println("Patient Not Found")

    }
    showPatientsMenu()

}

fun editPatient() {

    print("Enter Patient Name: ")
    val eName = readLine()
    if (eName == "") {

        println("Enter Correct Name")
        editPatient()

    } else {

        var isFound = false

        for ((i, name) in patientsArray.withIndex()) {

            if (name != null) {
                if (name.contains(eName.toString())) {

                    print("Patient Found, Enter New Name: ")
                    val newName: String? = readLine() ?: "Unknown Name"

                    print("Enter New Patient Number: ")

                    var newNum: Int? = 0
                    try {

                        newNum = readLine()?.toInt() ?: 0

                    } catch (e: NumberFormatException) {

                    }

                    patientsArray[i] = "$newName  patient id = $newNum"
                    println("Patient Edit")
                    isFound = true
                    break

                }
            }

        }
        if (!isFound)
            println("Nothing Found")

    }
    showPatientsMenu()

}

fun addPatient() {
    print("Enter Patient Name: ")
    val name: String = readLine()?.toString() ?: "Unknown Name"
    if (name != "") {

        var patientNum: Int? = 0

        print("Enter Patient Number: ")
        try {

            patientNum = readLine()?.toInt() ?: 0

        } catch (e: NumberFormatException) {

            println("Enter Correct Number")
            addPatient()

        }

        patientsArray[i] = "$name  patient id = $patientNum"
        ++i
        println("patient Add")
        showPatientsMenu()
    } else {
        println("Enter Correct Name")
        addPatient()
    }

}

fun showPatientsMenu() {
    println("\n\n**************Patients Tab**************\n")
    println(
        """1) Add a Patient
        |2) Edit a Patient Info
        |3) Delete a Patient
        |4) Show All Patients
        |5) Go To Main Menu
        |6) Exit
    """.trimMargin()
    )

    print("Enter a Number: ")

    var pNum: Int = 0

    try {
        pNum = readLine()?.toInt() ?: 0
    } catch (e: NumberFormatException) {
        println("Enter Correct Number")
        showPatientsMenu()
    }

    when (pNum) {
        0 -> showPatientsMenu()
        1 -> addPatient()
        2 -> editPatient()
        3 -> deletePatient()
        4 -> showPatients()
        5 -> showMainMenu()
        6 -> exit()
        else -> showMainMenu()
    }

}

fun showPatients() {
    var isAllNull = true

    for (b in patientsArray.indices) {
        if (patientsArray[b] != null) {
            println(patientsArray[b])
            isAllNull = false
        }

    }

    if (isAllNull) {
        println("Nothing To Show\n\n")
    }

    showPatientsMenu()
}

fun exit() {
    print("Hope you enjoy this app :)")
    exitProcess(1)
}
