package online.world.kananikotlin.hospitalmanagementwithoop

class Specialist(
    override val fullName: String,
    override val nationalCode: Int,
    private val medicalSystemNumber: Int,
    private val visitPrice: Int,
    private val specialty: String
) : Person {

    override fun printInformation(): String {
        return """Specialist FullName : $fullName, 
|NationalCode : $nationalCode 
|medicalSystemNumber : $medicalSystemNumber
|VisitPrice : $visitPrice
|Specialty : $specialty""".trimMargin()
    }

}