package online.world.kananikotlin.hospitalmanagementwithoop

class Hospital(val name: String) {

    companion object {
        private var hospitalNumber = 0

        fun hospitalNumber(): Int {
            return hospitalNumber
        }

    }

    init {
        ++hospitalNumber
    }

    var patientsList = mutableListOf<Patient>()
    var specialist = mutableListOf<Specialist>()
    var familyDoctor = mutableListOf<Patient>()

}