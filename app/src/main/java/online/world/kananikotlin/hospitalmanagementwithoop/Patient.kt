package online.world.kananikotlin.hospitalmanagementwithoop

class Patient(
    override val fullName: String,
    override val nationalCode: Int,
    val insuranceNumber: Int
) : Person {

    companion object {
        var number = 0;

        fun patientsNumber(): Int {
            return number
        }
    }

    init {
        ++number
    }

    override fun printInformation(): String {
        return """Patient FullName : $fullName, 
|NationalCode : $nationalCode 
|InsuranceNumber : $insuranceNumber""".trimMargin()
    }


}