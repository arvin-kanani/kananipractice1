package online.world.kananikotlin.hospitalmanagementwithoop

interface Person {

    val fullName: String
    val nationalCode: Int

    fun printInformation(): String

}