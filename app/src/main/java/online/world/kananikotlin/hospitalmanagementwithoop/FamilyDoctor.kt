package online.world.kananikotlin.hospitalmanagementwithoop

class FamilyDoctor(
    override val fullName: String,
    override val nationalCode: Int,
    private val medicalSystemNumber: Int,
    private val visitPrice: Int
) : Person {

    var doctorPatients = mutableMapOf<String, Int>()

    override fun printInformation(): String {

        var patients: String = ""

        for (a in 1..doctorPatients.size) {
            patients += "\name: ${doctorPatients.keys}  number: ${doctorPatients.values}"
        }

        return """Specialist FullName : $fullName, 
|NationalCode : $nationalCode 
|medicalSystemNumber : $medicalSystemNumber
|VisitPrice : $visitPrice
|patients: $patients""".trimMargin()

    }

}