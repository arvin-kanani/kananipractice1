package online.world.kananikotlin.previousSessionPractices

//Get Circle Area or Perimeter
fun main() {

    print("Enter Circle Radius: ")
    val rad: Double? = readLine()?.toDouble() ?:0.0


    print("Enter Action: ")
    val retVal: String? = readLine()

    println("Number is: ${checkCircleValue(
        rad,
        retVal
    )}")

}

fun checkCircleValue(radius: Double?, returnVal: String?): String {
    if (returnVal == "perimeter") {

        if (radius != 0.0) {
            return ((radius?.times(2) ?: return "Enter Correct Number") * Math.PI).toString()
        }

    } else if (returnVal == "area") {

        if (radius != 0.0) {
            return ((radius?.times(radius) ?: return "Enter Correct Number") * Math.PI).toString()
        }

    } else {

        return "Enter Correct Number"

    }
    return "Error While Getting Data"
}