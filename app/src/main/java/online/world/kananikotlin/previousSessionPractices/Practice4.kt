package online.world.kananikotlin.previousSessionPractices

//Calculate the number of iran in a String
fun main() {

    print("Enter A Text :")
    println(
        checkIran(
            readLine()?.toString() ?: "Enter A Correct Text Please"
        )
    )
}

var num = 0
fun checkIran(text: String): Int? {
    val lowerCases = text.toLowerCase()
    if (lowerCases.contains("iran")) {
        ++num
        checkIran(
            lowerCases.substringAfter(
                "iran"
            )
        )
    }
    return num
}